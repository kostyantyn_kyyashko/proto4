<?php
function sema4($attr)
{
    $title = isset($attr['title'])?$attr['title']:'Goals2';
    $wp_user_id = isset($attr['wp_user_id'])?$attr['wp_user_id']:null;
    if($wp_user_id) {
        $addSQL = " WHERE wp_user_id=$wp_user_id";
    }
    else {
        $addSQL = '';
    }
global $wpdb;
    if($addSQL) {
        $sql = "SELECT ca.status, COUNT(ca.status) counter FROM crm_analysis ca WHERE ca.wp_user_id={$wp_user_id} GROUP BY ca.status";
    }
    else {
        $sql = "SELECT ca.status, COUNT(ca.status) counter FROM crm_analysis ca GROUP BY ca.status ";
    }


$result = $wpdb->get_results($sql);
$out = [];
foreach ($result as $row) {
    $out[$row->status] =$row->counter;
}
$sql2 = "SELECT COUNT(*) FROM crm_analysis " . $addSQL;
$counter = $wpdb->get_var($sql2);
/*echo '<pre>';
var_dump($out); die();*/
?>
<div class="col-md-12">
    <p class="text-center">
        <h3><?=$title?></h3>
    </p>
    <?if ($counter == 0) echo '<h2>No records in DB from your account</h2>'?>
    <div class="progress-group">
        Begin
        <span class="float-right"><b><?=$out['Begin']?></b>/<?=$counter?></span>
        <?
        $width = round(($out['Begin']/$counter)*100);
        ?>
        <div class="progress progress-sm">
            <div class="progress-bar bg-default" style="width: <?=$width?>%"></div>
        </div>
    </div>
    <div class="progress-group">
        Analysis
        <?if (!isset($out['Analysis'])) $out['Analysis'] = 0?>
        <span class="float-right"><b><?=$out['Analysis']?></b>/<?=$counter?></span>
        <?
        $width = round(($out['Analysis']/$counter)*100);
        ?>
        <div class="progress progress-sm">
            <div class="progress-bar bg-warning" style="width: <?=$width?>%"></div>
        </div>
    </div>
    <div class="progress-group">
        Proposal
        <?if (!isset($out['Proposal'])) $out['Proposal'] = 0?>
        <span class="float-right"><b><?=$out['Proposal']?></b>/<?=$counter?></span>
        <?
        $width = round(($out['Proposal']/$counter)*100);
        ?>
        <div class="progress progress-sm">
            <div class="progress-bar bg-danger" style="width: <?=$width?>%"></div>
        </div>
    </div>
    <div class="progress-group">
        Negotiations
        <?if (!isset($out['Negotiations'])) $out['Negotiations'] = 0?>
        <span class="float-right"><b><?=$out['Negotiations']?></b>/<?=$counter?></span>
        <?
        $width = round(($out['Negotiations']/$counter)*100);
        ?>
        <div class="progress progress-sm">
            <div class="progress-bar bg-info" style="width: <?=$width?>%"></div>
        </div>
    </div>
    <div class="progress-group">
        Close
        <?if (!isset($out['Close'])) $out['Close'] = 0?>
        <span class="float-right"><b><?=$out['Close']?></b>/<?=$counter?></span>
        <?
        $width = round(($out['Close']/$counter)*100);
        ?>
        <div class="progress progress-sm">
            <div class="progress-bar bg-info" style="width: <?=$width?>%"></div>
        </div>
    </div>
    <!-- /.progress-group -->
    <!-- /.progress-group -->
</div>
<?
}
add_shortcode('sema4', 'sema4');