<?php

function main_output() {
    ?>
    <style>
        #wpadminbar {display: none}
        body {margin-top: -30px !important;}
    </style>
    <?
    $html = file_get_contents((dirname(dirname(dirname(__FILE__)))) . '/plugins/relotis-crm/header.html');
    $html .= file_get_contents((dirname(dirname(dirname(__FILE__)))) . '/plugins/relotis-crm/index1.php');
    ob_start();
    user_name();
    $user_name = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[user_name]', $user_name, $html);
    ob_start();
    close_data();
    $close_data = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[close_data]', $close_data, $html);
    ob_start();
    ammount();
    $ammount = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[ammount]', $ammount, $html);
    ob_start();
    opportunity_owner();
    $opportunity_owner = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[opportunity_owner]', $opportunity_owner, $html);
    ob_start();
    analysis();
    $analysis = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[analysis]', $analysis, $html);
    ob_start();
    contacts();
    $contacts = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[contacts]', $contacts, $html);
    ob_start();
    proposal();
    $proposal = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[proposal]', $proposal, $html);
    ob_start();
    negotiation();
    $negotiation = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[negotiation]', $negotiation, $html);
    ob_start();
    close();
    $close = ob_get_contents();
    ob_end_clean();
    $html = str_replace('[close]', $close, $html);
    $current_user = wp_get_current_user()->user_login;
    $html = str_replace('[current_user]', $current_user, $html);
    echo $html;
}
